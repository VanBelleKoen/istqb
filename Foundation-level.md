# Foundation level

Providing a basic understanding of how testing should be conducted. And keywords used inside the sector.

## Fundamentals of Testing

*Keywords*: Coverage, debugging, defect, error, failure, quality, quality assurance, root cause, test analysis, test basis, test case, test completion, test condition, test control, test data, test design, test execution, test implementation, test monitoring, test object, test objective, test oracle, test planning, test procedure, test process, test suite, testing, testware, traceability, validation, verification

### What is testing

Testing involves a great deal more then just executing tests. Planning, design, analyses, validation, verification and reporting are just a few of the activities that a tester will perform.

#### Typical objectives of Testing

    - Assess quality of the software
    - Reduce risk of software failure
    - Planning, analyzing, designing, implementing and executing tests
    - Gathering tests results
    - Both static and component testing
    - Review different work products
        - User Stories, requirements, source code ...
    - User acceptance of the software
    - Increase confidence in the test objects
    - Risk assessment before a release

#### Testing and debugging

    - Testing
        - Finding the failures caused by defects in the software
    - Debugging
        - Finding the actual defects
        - analyzing the defects
        - Fixing the defects

Testers can in involved in the debugging stage of development, and in agile teams this is most likely the case. In several situation it is benefital to the teams that a tester starts the debugging process in order to file a more complete bug report. This is also a great way to save the developers some work.

### Why is Testing Necessary

    - Contributing to the quality of the components
    - Reducing the risk of failures
    - Validating if certain industry standards have been reached

#### Testings contributions to success

    - Identification and removal of requirement defects
    - Reducing the risk of fundamental design defects
    - Reducing the risk of defects in the code
    - Validate the software to meet stakeholder requirements
    - Assisting in the removal of defects that cause failures

#### Quality Assurance and Testing

Quality Assurance governs all process that related to the quality of the final product, meaning that testing is only a smaller part of this.

#### Errors, Defects and Failures

The relationship between these terms is fairly straightforwards, an error can lead to a defect and a defect in turn can lead to a failure of the work product.
For example:
    An error in the design can lead to a defect in the requirements, which will lead to a programming error, which in turn could lead to a defect in the software. Defects in the software can cause failures.

During testing both false positives and false negatives can occur. In the first instance defects are reported that aren't actual defects. And in the case of false negatives, no defects are reported while they should have been.

### Seven principles of testing

    1. Testing shows the presence of defects
    2. Exhaustive testing is impossible
        2.1. Risk analyses and priorities manage this
    3. Early testing saves time and money
        3.1. Also known as Shift left
        3.2. Reducing costly changes and defects
    4. Defects cluster together
        4.1. Usually a small number of components have the most defects
    5. Beware of the pesticide paradox
        5.1. Tests needs to evolve together with the application
    6. Testing is context-dependent
    7. Absence of errors is a fallacy
        7.1. It is impossible to find all defects

### Test-Process

#### Test-Process in context

These factor influence the testing process for an organisation.

    - Contextual factors
        - Project methodology
    - Test levels
    - Product and Project risks
    - Operational constrains
      - Budget, timescale, complexity
    - Organizational policies and practices
    - Internal and external standards

#### Test Activities and Tasks

    - Test Planning
        - Defining the objectives for testing, in regard with the constrains of  the application
        - Test plans can be revisited based on feedback and monitoring
    - Test Monitoring and Control
        - Monitoring is the on-going comparison between the progress and the planned progress, while control involves reaching the objectives of the Test plan. The results of monitoring can be communicated using test progress reports.
    - Test Analysis
        - What to test ?
        - Used to identify testable features
        - Analyze the test basis to the test level
    - Test Design
        - How to test ?
        - Designing and prioritizing the different test cases
        - Identify necessary test data
        - Designing the test environment
        - Capturing the bi-directional traceability
    - Test Implementation
        - Do we have everything to test ?
        - Developing and creating test cases and automated tests
        - Creating the test suites based on the test cases
        - Arranging the test suites in an execution schedule
        - Preparing the test data
        - Verifying and updating the bi-directional traceability
        - Test Implementation and Test Design are often intermixed
    - Text Execution
        - Time to test
        - Test suite are run as defined in the test schedule
        - Execution, recording and comparing the test results
        - Reporting defects
    - Test Completion
        - Report on the test
        - Occurs at project milestone
        - Checks the progress towards defects resolution
        - Analyzing and using the lessons learned

#### Test work products

The test work products refer to the results of the activities and tasks as described above.

#### Traceability between the Test Basis and Test Work Products

Good traceability of the tests require the following criteria:
    - Analyzing the impact of changes
    - Testing should deliver reports
    - Using the IT governance criteria
    - Increased understandability of the test progress
    - Relating technical aspects of testing to stakeholders
    - Providing information to assess the project and product in terms of quality and progress

### Psychology of testing

#### Human psychology and testing

Testers should be aware of confirmation bias and other cognitive biases during the development stages of the product. These biases might affect communication and team effort if mishandled. Communication has to be handled with care towards the following points:
    - Collaboration
    - Emphasize on the benefits of testing
    - Neutral form of communication
    - Empathy towards the rest of the team
    - Summarize what has been said in order to validate if everyone understands

#### Tester's and developer's mindsets

Testers think very differently from developers in terms of the end objective. Development is focused on designing and building the product, while testers are focused on validation and verification of the product with the requirements. Both mindsets are equally valuable in the development progress and both must borrow from the other person's skill set.

## Testing Throughout the software Development Life cycle

*Keywords*: Acceptance testing, alpha testing, beta testing, change-related testing, commercial off-the-shelf, component integration testing, component testing, confirmation testing, contractual testing, functional testing, impact analysis, integration testing, maintenance testing, non-functional testing, operational acceptance testing, regression testing, regulatory acceptance testing, sequential development model, system integration testing, system testing, test basis, test case, test environment, test level, test object, test objective, test type, user acceptance testing, white-box testing

### Software Development life cycles models

#### Software development and software testing

     - Every development activity has a corresponding test activity
     - A test level has a test objective specific to that level
     - Testing starts at the beginning of a development stage
     - Testers should be involved from the get-go in a project

#### Software development life cycle models in context

A tester must always keep an eye on the context in which the project will be delivered and for what it will be used.
    - System complexity
    - The different components might be part of a larger whole
    - Time to deliver the product
These influence the testers ability and Psychology in testing the different features or components.

### Test levels

The different levels are the following:
    - Component Testing
    - Integration Testing
    - System Testing
    - Acceptance Testing

These different levels will be characterized with the next attributes:
    - Specific objectives
    - Test basis
    - Test object
    - Typical defects
    - Specific approaches and responsibilities

#### Component Testing

Better known as unit testing, focuses on single components that are testable. This type of testing is done independent from the system, while still being able to test functional, non-functional and structural properties.

The is no real formal defect management during this stage. The issues are found quickly and fixed as quickly. These test are mostly written by the developers. Consider here the Test Driven Development way of thinking.

#### Integration Testing

During this phase of testing the focus is on the interaction between all the different components and systems. The scope of the integration increases the complexity of testing significantly. It also increases the difficult to isolate the defect to a specific component.

#### System Testing

After testing the interaction between all the different components, testing should focus on the system in it's entirely. Most companies will use these results to decide if an application is ready for production. During this phase the focus lies on end-to-end testing, validating if the end requirements are met of the application.

#### Acceptance Testing

Very comparable with system testing, the main difference being that the tests are preformed by the customer and not by the independent tester. Finding defects during acceptance testing can show a severe risk in going to production.

The main objective of acceptance testing in whichever form is increasing confidence in the application and in releasing.

Differences here is between User-Acceptance-Testing and Operational-Acceptance-Testing. UAT is testing from the stand-point of the user or customer, while OAT is done out of the percpective of someone from operations. How difficult is taking a back-up or other management tasks.

### Test Types

#### Functional Testing

Validation the functional requirements of the application based on the user stories and use cases as described. These tests can be done on every level of testing, with a different focus for every level.

#### Non-Functional Testing

Testing the performance and/or security of an application of component fall in the category of non-functional testing. These type of tests may required specialized knowledge of the application.

#### White-box Testing

During White-box testing, the structure and design of an application is being tested. In this sense code coverage of the unit tests can be checked. Community standard here is 85%. This code coverage means in what amount do the unit tests check the different functions of the application.

#### Change-related Testing

These include confirmation testing and/or regression testing. If the defect has been fixed and if the fix didn't break anything. Regression tests are great candidates for automated testing. As they involve a great deal of work for a possible small change.

#### Test Types and Test Levels

Any type of Test can be done on any test level. While this can be done, but it might not be required.

### Maintenance Testing

Any change made to a system in production needs to be tested and validated. The scope hereof is dependent on the degree of risk with the change, the size of the current system and the size of the change.

#### Triggers for maintenance

All triggers can be classified as migration and modification.

#### Impact analysis for Maintenance

The impact analysis should be done in order to validate if the change is needed. Both the intended consequences and drawbacks should be considered before altering the system. Regression testing can be used in order to find the unintended consequences of a change.

## Static Testing

*keywords*: Ad hoc review, Checklist-based review, dynamic testing, formal review, informal review, inspection, perspective-based reading, review, role-based review, scenario-based review, static analysis, static testing, technical review, walkthrough

### Static Testing Basics

Static testing focuses on the manual examination of the different work-products. No code will be executed during the static testing phase. This can be done with reviews or tool-driven analyses of the work-products.

#### Work products that can be examined by static testing

    - Specifications
    - User stories
    - Code
    - Test plans
    - User guides
    - Web Pages
    - Contracts
    - Configuration
    - Activity diagrams

#### Benefits of Static Testing

Considering that defects found earlier in the process are often cheaper and easier to fix, static testing offers a way to find certain defects quickly and very early in the process.

#### Differences between Static and Dynamic Testing

While the objective remains the same, the main difference between both is that during static testing the code is not executed. Rather both types of testing complement one and other. Another difference is that static testing focuses on consistency and internal quality while dynamic testing focuses more on functional and visible quality.

### Review Process

The objectives from the review can differ wildly based on the agreed objectives. Ranging from finding defects to educating new team-members.

#### Work product Review process

    - Planning
    - Initiate review
    - Individual review
    - Issue communication and analysis
    - Fixing and reporting

#### Roles and responsibilities in formal form

    - Author
    - Management
    - Facilitator
    - Review Leader
    - Reviewers
    - Scribe (recorder)

#### Review Types

    - Informal review
        - Generating new ideas or solutions
        - Very common in agile environments
        - Not based on a formal process
    - Walk-through
        - Find defects
        - Individual preparation
        - Potential defects are logged
        - Can range from informal to very formal
    - Technical review
        - Gaining consensus
        - Individual preparation is required
        - Scribe is mandatory (Preferably not the author)
    - Inspections
        - Detecting potential defects
        - Defined formal process
        - Clearly defined rules
        - Individual preparation required
        - Scribe is mandatory

#### Applying review techniques

    - Ad-Hoc
        - Little or no preparation
        - Dependent on the reviewer skill
    - Checklist-based
        - Systematic technique
        - Questions based on possible defects
        - Drawback is that defects outside the checklist are ignored
    - Scenarios and dry runs
        - Structured guidelines for the reviewers
    - Perspective-based
        - Reviewer based on the stakeholder perspective
        - Leads to more in-depth testing
    - Role-based
        - Reviewer based on the user perspective

#### Success factors for reviews

    - Clear objectives
    - Review type, review techniques and review levels are appropriate
    - Review happens in smaller chunks
    - Adequate time to prepare
    - Management supports the review process
    - Integrated in the quality and test policies

## Test Techniques

*Keywords*: Black-box test technique, boundary value analysis, checklist-based testing, coverage, decesion coverage, decesion table testing, error guessing, equivalence partitioning, experience-based test technique, exploratory testing, state transition testing, statement coverage, test technique, use case testing, white-box testing

### Categories of Test Techniques

Several factors influence the choice of techniques:

    - Component
    - Regulatory standard
    - Risk level
    - Available tools, budget, time and documentation
    - Tester knowledge and skill
    - Software development life-cycle model
    - The types of defects expected in the component

#### Categories of test techniques and their characteristics

Black-box testing

    - Behavioral testing technique
    - Applicable to both functional and non-functional test
    - There is no reference to the internal structure

White-box testing

    - Structure based testing
    - Concentration on the internal structure of the application

Experience-based testing

    - Often combines the Black-box and white-box testing

### Black-box Testing

#### Equivalence Partitioning

Data is being divided in partitions, each data point in this partition should be processed by the system in the same way. These partitions can be identified by an data element, regardless of input our output.

#### Boundary value analysis

When the partition is ordered, it becomes possible to use boundary value analysis. It becomes possible to use a minimum and maximum value. To use invalid values on both sides of the partition. If an boundary value is 1 for instance. The test values would be 0, 1 and 2.

#### Decision table testing

When dealing with complex rules inside the system, testers should consider making a decision matrix in order to have a visual representation of the flow. A full table would include every possible combination. There can be deleted before actual testing.

#### State transition testing

Tests should be designed in order to validate all the identified state transitions inside the application. Coverage would be seen as the number of tested states divided by the number of known state transitions.

#### Use case testing

Specific ways of interacting with the application based on the intended interactions. These interactions can include negative and erroneous behavior.

### White-box testing

#### Statement testing and coverage

Testing of the individual statements in the code. This is where the code coverage metric is used.

#### Decision testing and coverage

If there are decision trees in the code, a test design should take both a valid and invalid outcome into account. So every if statement or case switch should have all possible outcomes tested.

100% decision testing coverage equals 100% statement testing coverage but never the other way around.

### Experience based testing

#### Error guessing

Testing based on the anticipation of errors and issues. This type of testing relies heavily on the skillset and experience of the tester.

#### Exploratory testing

The test cases are logged during the actual test execution, prompting the tester to take less obvious courses.

#### Checklist-based testing

Testing based on a checklist that was prepared in advance. The checklist itself provides guidelines and reduced the repeatability of the specific test. On the other hand does the checklist improve coverage.

## Test Management

*Keywords*: Configuration management, defect management, defect report, entry criteria, exit criteria, product risk, project risk, risk, risk level, risk-based testing, test approach, test control, test estimation, test manager, test monitoring, test plan, test planning, test progress report, test strategy, test summary report, tester

### Test Organization

#### Independent testing

Potential benefits for independent testing

    - Different background and percpectives from the developers
    - Easier to challenge, verify and disprove assumptions made by stakeholders
    - No political pressure from the company

Potentional drawbacks for independent testing

    - Isolation from the development team, which may lead to lack of collaboration
    - Detached sense of responsibility
    - Independent testers can be seen as a bottleneck
    - Independent testers might lack some information

#### Tasks of a test manager and a tester

Test manager:

    - Develop and review test policy
    - Coordinate the test plan
    - Plan the different test activities
    - Decide about the implimentation of a test environment
    - Promote and advocate the testers, the test team and general testing in the organisation
    - Develop the skills and carriers of the testers
  
Testers:

    - Review and contribute to the test plan
    - Analyse and review the requirements
    - Prepare and acquire the test data
    - Execute the tests
    - Automate the tests as needed
    - Peer review the different tests

### Test planning and estimation

#### Purpose and content of a test plan

A test plan will outline the different test activities that are required for the projects. Test planning is seen as a continual activity during the development process. So new information will always be added to the test plan.

#### Test strategy and test approach

This is the general discription of the test process.

    - Analytical
    - Model-based
    - Methodical
    - Process-compliant
    - Directed
    - Regression-averse
    - Reactive

#### Entry criteria and exit criteria

Entry criteria

    - Availability of testable features
    - Availability of items that have passed the exit criteria for the previous test level
    - Availability of items required

Exit Criteria

    - Planned test are executed
    - The defined coverage level has been achieved
    - The unresolved defects are on the agreed limit
    - The reliability, performance, usability, security is sufficient

#### Test execution schedule

This will define the order in which the test suites are executed, usually based on the priority level.

#### Factors influencing the test effort

    - Product characteristics
    - Development process
    - People characteristics
    - Test results

#### Test estimation techniques

Two very different techniques are used in order to estimate the amount of testing, metric and expert. In the metric estimation the tester will fall back on previous projects in order to estimate, while in the expert estimation it will fall to the testers experience to make a correct estimation.

### Test monitoring and control

Test monitoring is all about gathering the information required to give feedback while test control is about guiding and corrective actions that need to be taken.

#### Metrics used int testing

    - Percentage done
    - Test case execution
    - Defect information
    - Task completion
    - Cost of testing

#### Purpose, contents, and audiences for test reports

Test reports are required in order to summarize and communicate about the test activities, this can be done during any stage of the development cycle. When the exit criteria are reached the test manager should issue a test report. This would include a full overview of the testing that has been performed and how. In regard with the test plan and any deviations of the test plan, in order to point towards factors that impede the progress or even blocked it. The test report should be used as a tool to increase the testing preformance or the suitability of the organisation to test.

### Configuration management

Configuration management is used in order to provide a degree of integrity to the testware, this includes version control and different changes that were implemented.

### Risks and testing

#### Definition of risk

Risk involves any event in the future with a possiblity of a negative consequence. The level of risk is determinated by the likelihood of the event and the impact.

#### Product and project risks

Product risks are called quality risks when the specific quality characteristics are impacted.  While project risks are more due to the organisation. Meaning they only qualify for risks that impact the projects capability to achieve the objective.

### Defect management

Defect that are found during any level of testing should be logged as completly as possible for the developers to either find the issues or reproduce the bug. This would include in the analyses and static code reviews.

## Tool support for testing

*Keyword*: Data-driven testing, keyword-driven testing, test automation, test execution tool, test management tool

### Test tool considerations

These tools are used to support one or more of the test activities, either directly or indirectly.

#### Test tool classification

    - Tool support for management of testing and testware
      - Test case management tools
    - Tool support for static testing
      - Code analysis
    - Tool support for test design and implenentation
      - Test data preparation tools
    - Tool support for test execution and logging
    - Tool support for performance measurement and dynamic analysis
    - Tool support for specialized testing needs

#### Benefits and risks of test automation

Benefits of test automation:

    - Reducing time needed for regression testing
    - Easier repeatability of the tests
    - Objective assessement
    - Easier reporting

Drawbacks of test automation:

    - Unrealistic expectations
    - Underestimation of the cost
    - Maintance requires time and effort
    - Over reliance on the tooling
    - The tool itself could be discontinued or outdated or lack of support.
