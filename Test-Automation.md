# Test Automation Engineer Advanced Syllabus

- [Test Automation Engineer Advanced Syllabus](#test-automation-engineer-advanced-syllabus)
  - [1. Introduction and objectives for test automation](#1-introduction-and-objectives-for-test-automation)
    - [1.1 Purpose of test automation](#11-purpose-of-test-automation)
    - [1.2 Success factors in test automation](#12-success-factors-in-test-automation)
  - [2. Preparing for Test Automation](#2-preparing-for-test-automation)
    - [2.1 SUT factors influencing test automation](#21-sut-factors-influencing-test-automation)
    - [2.2 Evaluation and selection](#22-evaluation-and-selection)
    - [2.3 Design for testability and automation](#23-design-for-testability-and-automation)
  - [3. The Generic Test Automation Architecture (gTAA)](#3-the-generic-test-automation-architecture-gtaa)
    - [3.1 Introduction to gTAA](#31-introduction-to-gtaa)
      - [3.1.1 Overview of the gTAA](#311-overview-of-the-gtaa)
    - [3.2 TAA design](#32-taa-design)
    - [3.3 TAS development](#33-tas-development)
  - [4. Deployment risks and contingencies](#4-deployment-risks-and-contingencies)
    - [4.1 Selection of test automation approach and planning of deployment/rollout](#41-selection-of-test-automation-approach-and-planning-of-deploymentrollout)
      - [4.1.1 Pilot project](#411-pilot-project)
      - [4.1.2 Deployment project](#412-deployment-project)
    - [4.2 Risk assessment and mitigation strategies](#42-risk-assessment-and-mitigation-strategies)
    - [4.3 Test automation Maintenance](#43-test-automation-maintenance)
  - [5. Test automation reporting and metrics](#5-test-automation-reporting-and-metrics)
    - [5.1 Selection of TAS metrics](#51-selection-of-tas-metrics)
    - [5.2 Implementation of measurement](#52-implementation-of-measurement)
    - [5.3 Logging of the TAS and the SUT](#53-logging-of-the-tas-and-the-sut)
    - [5.4 Test automation reporting](#54-test-automation-reporting)
  - [6 Transitioning manual testing to an automated environment](#6-transitioning-manual-testing-to-an-automated-environment)
    - [6.1 Criteria for Automation](#61-criteria-for-automation)
    - [6.2 Identify steps needed to implement automation within regression testing](#62-identify-steps-needed-to-implement-automation-within-regression-testing)
    - [6.3 Factors to consider when implementing automation within new feature testing](#63-factors-to-consider-when-implementing-automation-within-new-feature-testing)
    - [6.4 Factors to consider when implementing automation of confirmation testing](#64-factors-to-consider-when-implementing-automation-of-confirmation-testing)
  - [7. Verifying the TAS](#7-verifying-the-tas)
    - [7.1 Verifying the automated test environment components](#71-verifying-the-automated-test-environment-components)
    - [7.2 Verifying the automated test suite](#72-verifying-the-automated-test-suite)
  - [8. Continous improvement](#8-continous-improvement)
    - [8.1 Options for improving test automation](#81-options-for-improving-test-automation)
  - [8.2 Planning the implementation of test automation improvement](#82-planning-the-implementation-of-test-automation-improvement)

## 1. Introduction and objectives for test automation

*Keywords:* 
> API testing, CLI testing, GUI testing, System under test (SUT), Test Automation architecture, test automation framework, test automation strategy, test automation, test script, testware

### 1.1 Purpose of test automation

In order to develop testware, five points need to be taken into account.

    - Software
    - Documentation
    - Test Cases
    - Test Environments
    - Test Data

In test automation three different tasks are required, setuping the software to test and the preconditions that are needed, executing the tests and lastly comparing the results with the predicted results. In line with the different tasks of test automation the testware is required to handle three things, implementation of test cases, monitoring and controlling the testcases and interpretting and rapporting the results.

The interaction with the SUT is done in three different approaches.

    - Public interfaces (API testing)
    - User interfaces of the SUT (GUI testing)
    - Through a service or a protocol

Test automation has the following objectives

    - Improving test efficiency
    - Providing wider functional coverage
    - Reducing the total test cost
    - Performing tests that manual testers cannot
    - Shortening the test exectution period
    - Increasing the test frequency

| Advantage | Disadvantage | Limitations |
|---|---|---|
|More tests | Additional costs | Not all can be automated |
|More advanced tests | Initial setup cost | Can only verify machine-interpretable results
|Reducing total test cost|Requires additional technologies|Can only check actual results|
|Tests with higher complexity|Requires development skills|Not a replacement for exploratory testing|
|Less subject to operator error|TAS maintenaince||
|More efficient and effective use of resources|Can distract from test objectives||
|Quicker feedback|Tests can become too complex||
|Improves system reliability|Errors introducted by automation||
|Improved consistency of tests|||

### 1.2 Success factors in test automation

Major factors in the success of test automation

    - Test automation architecture (TAA)
      - Designed for maintainability, performance and learnability
      - Supporting both the functional and non-functional requirements
    - SUT testability
    - Test Automation strategy (TAS)
      - Adresses the maintanability and consistency of the SUT
    - Test Automation Framework (TAF)
      - Easy to use
      - Easy to maintenain
      - Well documented

## 2. Preparing for Test Automation

*Keywords:*
> Testability, driver, level of intrusion, stub, test execution tool, test hook, test automation manager

### 2.1 SUT factors influencing test automation

    - SUT interfaces
    - Third party software
    - Levels of intrusion
      - The test might influences the SUT causing different errors
    - Different SUT architecture
    - Size and complexity of the SUT

### 2.2 Evaluation and selection

It is the responsiblity of the Test Automation Manager to assess the different tools.

    - Assessing organizational maturity
    - Assessing appropriate objectives
    - Identifying and collecting information
    - Analyzing tool information
    - Estimating cost-benefit ratio
    - Making a recommendation
    - Identifying compatibility of the tool with SUT

### 2.3 Design for testability and automation

SUT design for testability should always encompass several parts

    - Observability
    - Controlability
    - A clearly defined architecture

## 3. The Generic Test Automation Architecture (gTAA)

*Keywords:*
>Capture/playback, data-driven testing, generic testing automation architecture, keyword-driven testing, linear scripting, model-based testing, process-driven scripting, structured scripting, test adaptation layer, test automation architecture, test automation framework, test automation solution, test definition layer, test execution layer, test generation layer

### 3.1 Introduction to gTAA

The gTAA allows for a structured and more modular approach.

    - Single responsibility components
    - Open for extensions but closed for motification
    - Replaceable (Substition principle)
    - Component segregation
    - Dependency inversion

Based on a TAS the gTAA will be implemented by tools, plugins and components.

#### 3.1.1 Overview of the gTAA

    - Test Generation
      - Test models and Manual Design
    - Test Definition
      - Test conditions, Test Cases, Test procedures
      - Test Data
      - Test libary
    - Test Execution
      - Test Execution
      - Test logging, test reporting
    - Test Adaptation
      - GUI, API testing
    - Interface with project management

### 3.2 TAA design

Principle activities required in a TAA

    - Capture requirements needed to define an appropiate TAA
    - Compare and contrast different design/architecture appraoches
    - Identify areas where abstraction can deliver benefits
    - Understand SUT technologies and how these interconnect with the TAS
    - Understand the SUT environment
    - Time and complexity for a given testware architecture implementation
    - Ease of use for a given testware architecture implementation

Approaches to Automating test cases

    - Capture/playback approach
      - Record and play
    - Linear scripting
      - Each full flow has it's own script
    - Structured scripting
      - The flow is divided in parts to script
    - Data-driven testing
      - There are data files
    - Keyword-driven testing
      - The keywords are fundamental in order to launch the tests
    - Process-driven scripting
      - Built on top of keyword-driven testing
      - User-based scenario's
    - Model-based testing
      - Automated generation of test cases
      - Focus lies more on the execution and less on the design

### 3.3 TAS development

There has to be compatibility between the TAS and the SUT

    - Process compatibility
    - Team compatibility
    - Technology compatibility
    - Tool compatibility

## 4. Deployment risks and contingencies

*Keywords:*
> Risk, risk mitigation, risk assessment, product risk

### 4.1 Selection of test automation approach and planning of deployment/rollout

#### 4.1.1 Pilot project

    - Used in order to prove that the TAS is able to achieve the objectives
      - Learn the details about the TAS
      - See how it fits with current processes, procedures and tools
      - Design the automation interface
      - Decide on standards
      - Identifying metrics
      - Assess if the benefits are doable within certain cost
      - Determine the required skillset
  
    - Project process for the pilot
      - Neither a critical nor a trivial project should be taken
      - Involve the different stakeholders
      - SUT is a good reference for the rest of the other projects
      - Treated as a regular development project

#### 4.1.2 Deployment project

    - Rollout for the other projects
      - Incrementally
      - Adapting and improving during the rollout
      - Providing training for the new users
      - Defining usage guidelines
      - Gathering information about the actual use and monitoring the actual use
      - Providing support
      - Gathering lessons learned
      - Identifying and implementing improvements

### 4.2 Risk assessment and mitigation strategies

Different type of issues

    - Technical issue
    - Typical deployment
    - Potential failure points

### 4.3 Test automation Maintenance

Different Types of maintenance

    - Preventive maintenance
    - Corrective maintenance
    - Perfective maintenance
      - Non-functional fixes
    - Adaptive maintenance

## 5. Test automation reporting and metrics

*Keywords:*
> Automation code defect, density, coverage, tracebility matrix, equivalent manual test effort, metrics, test logging, test reporting

### 5.1 Selection of TAS metrics

TAS metrics can be both internal and external

    - External TAS metric
      - Automation benefit
      - Effort to build tests
      - Effort to analyze incidents
      - Effort to maintain
      - Ratio of failures to defects
      - Time to execute tests
      - Number of automated test cases
      - Number of pass and fail results
      - Number of false-fails and false-pass
      - code coverage
    - Internal TAS metric
      - Tool scripting metric
      - Automation code defect density
      - Speed and efficiency of TAS components

### 5.2 Implementation of measurement

Most testware tools will support recording and logging of information during the test execution. These can be integrated with third party tools and provided visualization for those results, such as graphs, charts and dashboards.

### 5.3 Logging of the TAS and the SUT

Logging is a sources to troubleshoot potential defects and issues inside of the TAS and the SUT.

### 5.4 Test automation reporting

    - Content of the report
      - Summary of the execution results
    - Publishing the report

## 6 Transitioning manual testing to an automated environment

*Keywords:*
> Confirmation testing, Regression testing

### 6.1 Criteria for Automation

Some suitable criteria are the following:

  - Frequency of use
  - Complexity to automate
  - Compatibility of tool support
  - Maturity of test process
  - Automate-able in SDLC
  - Automated environment maintenable
  - Controlablility of the SUT
  - Technical planning in support of ROI
  - Availablility of tools for automation
  - Correctness of test data
  - Scope of automation effort
  - Parallel effort
  - Automation reporting

### 6.2 Identify steps needed to implement automation within regression testing

  - Frequency of test execution
  - Test execution time
  - Functional overlap
  - Data sharing
  - Test interdependency
  - Test preconditions
  - SUT coverage
  - Executables tests
    - The manual test must be correct and automatable
  - Large regression test sets

### 6.3 Factors to consider when implementing automation within new feature testing

Automation of new features is a constant process to validate and the tools used in automation. When the design of the new features comes into play the TAE can adapt the new feature to be testable with the current tools or consider changing the tools used.

### 6.4 Factors to consider when implementing automation of confirmation testing

Extended confirmation testing allows to add additional logging and reporting in order to track defects faster.

## 7. Verifying the TAS

*Keywords:*
> Verification

### 7.1 Verifying the automated test environment components

Each of the different components of the TAS must be verified that they work as intended.

  - Test tool installation, setup, configuration, and customization
  - Test scripts with known passes and failures
  - Repeatability in setup/teardown of the test environment
    - Systematic and well-documented approach is needed
  - Configuration of the test environment and components
  - Connectivity against internal and external systems/interfaces
  - Intrusiveness of automated test tools
    - High intrusiveness can influence the tests and can cause failures
  - Framework component testing
    - All software has to be tested

### 7.2 Verifying the automated test suite

  - Executing test scripts with known passes and failures
  - Checking the test suite
  - Verifying new tests that focus on new feature of the framework
  - Considering repeatability of tests
  - Checking if there are enough verification points in the automated test suite

## 8. Continous improvement

*Keywords:*
> Maintenance

### 8.1 Options for improving test automation

  - Scripting
    - Establish error recovery
    - Evaluate wait procedure
      - Best options is event mechanism
  - Test execution
  - Verification
    - Adding more verification functions
  - Architecture
  - Pre- and post-processing
  - Documentation
  - TAS features
    - Adding reporting, logging and other integration into different systems
  - TAS upgrades and updates

## 8.2 Planning the implementation of test automation improvement

  - Identify changes in the test environment components
  - Increase efficiency and effectiveness
  - Target multiple functions
  - Refactor the TAA in order to adapt to changes in the SUT
  - Naming conventions and standardization
  - Evaluation of existing scripts